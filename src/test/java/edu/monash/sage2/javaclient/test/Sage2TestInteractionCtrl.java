package edu.monash.sage2.javaclient.test;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseMotionAdapter;

import javax.swing.JCheckBox;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import edu.monash.sage2.javaclient.Sage2WebsocketIOClient;
import edu.monash.sage2.javaclient.modules.Sage2InteractionModule;

public class Sage2TestInteractionCtrl extends JFrame {

	
	
	private Sage2WebsocketIOClient client;
	private Sage2InteractionModule im;

	int oldx = -1;
	int oldy = -1;
	
	public Sage2TestInteractionCtrl() {
		
		init();
		
		setSize(400,  300);
		
		setVisible(true);
		
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
	}
	
	private void init() {
		
		client = new Sage2WebsocketIOClient();
		im = new Sage2InteractionModule();
		client.addModule(im);
		client.connect("wss://triohead.cave.monash.edu:443", null);
		
		im.registerInteractionClient("Eclipse", 255, 0, 0);

		if( ! client.isConnected())
			System.exit(0);
		
		JPanel panel = new JPanel() ;
		panel.setBackground(Color.WHITE);
		getContentPane().setLayout(new BorderLayout());
		getContentPane().add(panel, BorderLayout.CENTER);
		
		final JCheckBox checkbox = new JCheckBox("Enable Mousecursor");
		getContentPane().add(checkbox, BorderLayout.NORTH);
		checkbox.addChangeListener(new ChangeListener() {
			
			@Override
			public void stateChanged(ChangeEvent e) {
				if(checkbox.isSelected())
					im.startMouseCursor();
				else
					im.stopMouseCursor();
				
			}
		});
		
		
		panel.addMouseMotionListener(new MouseMotionAdapter() {

			@Override
			public void mouseMoved(MouseEvent e) {
				// init, first time mouseevent only
				if(oldx < 0)
					oldx = e.getX();
				if(oldy < 0)
					oldy = e.getY();
				
				
				im.moveMousePointer(e.getX() - oldx, e.getY() - oldy);
				
				oldx = e.getX();
				oldy = e.getY();
			}


			@Override
			public void mouseDragged(MouseEvent e) {
				// TODO Auto-generated method stub
				// init, first time mouseevent only
				if(oldx < 0)
					oldx = e.getX();
				if(oldy < 0)
					oldy = e.getY();
				
				
				im.moveMousePointer(e.getX() - oldx, e.getY() - oldy);
				
				oldx = e.getX();
				oldy = e.getY();
			}
			
			
		});
		panel.addMouseListener(new MouseAdapter() {

			@Override
			public void mousePressed(MouseEvent e) {
				im.pressMousePointer(e.getButton());
			}

			@Override
			public void mouseReleased(MouseEvent e) {
				im.releaseMousePointer(e.getButton());
			}

			
			
			
		});
		
		

	}
	
	public static void main(String[] args) {
		new Sage2TestInteractionCtrl();
	}
	
}

package edu.monash.sage2.javaclient.test;
import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.net.ConnectException;

import javax.imageio.ImageIO;

import org.json.JSONException;
import org.json.JSONObject;

import edu.monash.sage2.javaclient.Sage2EventListener;
import edu.monash.sage2.javaclient.Sage2WebsocketIOClient;


public class Sage2TestClientCtrl {

	
	private static String startMediaStream;

	public static void main(String[] args) {
		try {
			final Sage2WebsocketIOClient client = new Sage2WebsocketIOClient();
			
			
			client.on("initialize", new Sage2EventListener() {
				
				@Override
				public void onEvent(String data) {
					try {
						System.out.println("remote event: [initialize]: " + data);
						JSONObject obj = new JSONObject(data);
						String string = obj.getJSONObject("d").getString("UID");
						client.setClientUID(string);
					} catch (JSONException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
			});
			client.on("setupDisplayConfiguration", new Sage2EventListener() {
				
				@Override
				public void onEvent(String data) {
					System.out.println("remote event: [setupDisplayConfiguration]: " + data);
				}
			});
			client.on("requestNextFrame", new Sage2EventListener() {
				
				@Override
				public void onEvent(String data) {
					System.out.println("remote event: [requestNextFrame]: " + data);
					
					BufferedImage bi = new BufferedImage(100, 100, BufferedImage.TYPE_INT_ARGB);
					Graphics2D g = bi.createGraphics();
					g.clearRect(0, 0, 100, 100);
					g.setColor(Color.WHITE);
					g.drawLine(0, 0, 100, 100);
					ByteArrayOutputStream output = new ByteArrayOutputStream();
					try {
						ImageIO.write(bi, "jpeg", output);
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
			});
			client.on("eventInItem", new Sage2EventListener() {
				
				@Override
				public void onEvent(String data) {
					System.out.println("remote event: [eventInItem]: " + data);
				}
			});
			client.on("setItemPositionAndSize", new Sage2EventListener() {
				
				@Override
				public void onEvent(String data) {
					System.out.println("remote event: [setItemPositionAndSize]: " + data);
				}
			});
			client.on("finishedResize", new Sage2EventListener() {
				
				@Override
				public void onEvent(String data) {
					System.out.println("remote event: [finishedResize]: " + data);
				}
			});
			client.on("stopMediaStream", new Sage2EventListener() {
				
				@Override
				public void onEvent(String data) {
					System.out.println("remote event: [stopMediaStream]: " + data);
				}
			});
			
			
			client.connect("wss://triohead.cave.monash.edu:443", "sage2#1001");
			
			while(client.getClientUUID() == null)
				try {
					System.out.println("waiting...");
					Thread.sleep(500);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
//			startMediaStream = client.startMediaStream("My Stream", 100, 100);
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ConnectException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
	}
}

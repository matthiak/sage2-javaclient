package edu.monash.sage2.javaclient;

public interface Sage2EventListener {
	public void onEvent(String data);
}

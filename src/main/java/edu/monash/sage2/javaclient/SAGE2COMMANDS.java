package edu.monash.sage2.javaclient;

public enum SAGE2COMMANDS {
	registerInteractionClient,
	startSagePointer,
	stopSagePointer,
	pointerPress,
	pointerRelease,
	pointerDblClick,
	pointerPosition,
	pointerMove,
	pointerScrollStart,
	pointerScroll,
	pointerScrollEnd,
	pointerDraw,
	keyDown,
	keyUp,
	keyPress,
	uploadedFile,
	startNewMediaStream,
	updateMediaStreamFrame,
	updateMediaStreamChunk,
	startNewMediaBlockStream,
	updateMediaBlockStreamFrame,

	requestVideoFrame,
	receivedMediaStreamFrame,
	receivedRemoteMediaStreamFrame,

	receivedMediaBlockStreamFrame,

	receivedRemoteMediaBlockStreamFrame,

	finishedRenderingAppFrame,
	updateAppState,
	updateStateOptions,
	appResize,
	appFullscreen,
	broadcast,
	applicationRPC,
	requestAvailableApplications,

	requestStoredFiles,
	loadApplication,
	loadFileFromServer,
	deleteElementFromStoredFiles,

	moveElementFromStoredFiles,

	saveSesion,
	clearDisplay,
	tileApplications,
	radialMenuClick,
	radialMenuMoved,
	removeRadialMenu,
	radialMenuWindowToggle,
	addNewWebElement,
	openNewWebpage,
	playVideo,
	pauseVideo,
	stopVideo,
	updateVideoTime,
	muteVideo,
	unmuteVideo,
	loopVideo,
	addNewElementFromRemoteServer,

	addNewSharedElementFromRemoteServer,

	requestNextRemoteFrame,
	updateRemoteMediaStreamFrame,

	stopMediaStream,
	updateRemoteMediaBlockStreamFrame,

	stopMediaBlockStream,
	requestDataSharingSession,
	cancelDataSharingSession,
	acceptDataSharingSession,
	rejectDataSharingSession,
	createRemoteSagePointer,
	startRemoteSagePointer,
	stopRemoteSagePointer,
	remoteSagePointerPosition,
	remoteSagePointerToggleModes,

	remoteSagePointerHoverCorner,

	addNewRemoteElementInDataSharingPortal,

	updateApplicationOrder,
	startApplicationMove,
	startApplicationResize,
	updateApplicationPosition,
	updateApplicationPositionAndSize,

	finishApplicationMove,
	finishApplicationResize,
	deleteApplication,
	updateApplicationState,
	updateApplicationStateOptions,

	addNewControl,
	closeAppFromControl,
	hideWidgetFromControl,
	openRadialMenuFromControl,
	recordInnerGeometryForWidget,

	createAppClone,
	sage2Log,
	command,
	createFolder
}
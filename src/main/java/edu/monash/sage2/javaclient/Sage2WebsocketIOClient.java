package edu.monash.sage2.javaclient;
import java.io.IOException;
import java.net.ConnectException;
import java.net.URI;
import java.net.URISyntaxException;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.util.AbstractMap;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.java_websocket.client.WebSocketClient;
import org.java_websocket.handshake.ServerHandshake;
import org.json.JSONException;
import org.json.JSONObject;

import edu.monash.sage2.javaclient.helper.Dimension;
import edu.monash.sage2.javaclient.modules.Sage2ClientModuleInterface;
import edu.monash.sage2.javaclient.structs.Sage2DisplayConfig;


public class Sage2WebsocketIOClient {
	
	private static final Logger logger = Logger.getLogger(Sage2WebsocketIOClient.class);
	
	static {
		logger.setLevel(Level.OFF);
	}
	
	static int ALIASCOUNTER = 2;
	
	static int STREAMID = 1;
	
	/*
	 * maps function / event names (listeners) to numbers (alias)
	 * given by the sage2 system
	 * e.g. "initialize" > "0001"
	 */
	Map<String, String> funcLookupTableRemote = new HashMap<String, String>();
	
	/**
	 * maps alias number to function / event names 
	 * alloated locally
	 */
	Map<String, String> funcLookupTableLocal = new HashMap<String, String>();
	
	/**
	 * reverse map that maps function / event names to Alias
	 */
	Map<String, String> funcLookupTableLocalRev = new HashMap<String, String>();
	
	/*
	 * maps alias (event number) to listener
	 */
	Map<String, List<Sage2EventListener>> mapFuncNumberLocalListener = new HashMap<String, List<Sage2EventListener>>();
	
	protected WebSocketClient client;
	
	List<Map.Entry<String, JSONObject>> listRetryEmitItems; 
	
	String clientUID;

	Set<Sage2ClientModuleInterface> listModules;
	
	public static void enableLogging(boolean log) {
		if(log)
			logger.setLevel(Level.DEBUG);
		else
			logger.setLevel(Level.OFF);
	}
	
	public class Sage2VersionInformation {
		public String base;
		public String branch;
		public String commit;
		public String date;
		public Sage2VersionInformation(String base, String branch, String commit, String date) {
			this.base = base;
			this.branch = branch;
			this.commit = commit;
			this.date = date;
		}
	}
	
	Sage2VersionInformation sage2versioninfo;

	private String uri;

	protected Sage2DisplayConfig conf;

	private String password;
	
	
//	private String streamId;
	
	public Sage2WebsocketIOClient() {
		listModules = new HashSet<Sage2ClientModuleInterface>();

	}
	
	
	
	private void init() {
		funcLookupTableRemote = new HashMap<String, String>();
		funcLookupTableLocal = new HashMap<String, String>();
		funcLookupTableLocalRev = new HashMap<String, String>();
		
		listRetryEmitItems = new ArrayList<Map.Entry<String,JSONObject>>();
		
		
		
		try {
			TrustManager[] tmAllCerts = new TrustManager[]{
					new X509TrustManager() {

						@Override
						public X509Certificate[] getAcceptedIssuers() {
							// TODO Auto-generated method stub
							return null;
						}

						@Override
						public void checkServerTrusted(X509Certificate[] chain, String authType)
								throws CertificateException {
							// TODO Auto-generated method stub
							logger.debug("checkServerTrusted");
						}

						@Override
						public void checkClientTrusted(X509Certificate[] chain, String authType)
								throws CertificateException {
							// TODO Auto-generated method stub

						}
					}
			};
			SSLContext instance;
			instance = SSLContext.getInstance("SSL");
			instance.init(null, tmAllCerts, null);



			HostnameVerifier allHostsValid = new HostnameVerifier() {
				public boolean verify(String hostname, SSLSession session) {
					logger.debug("verify " + hostname);
					return true;
				}
			};
			HttpsURLConnection.setDefaultHostnameVerifier(allHostsValid);

			
			funcLookupTableRemote.put("#WSIO#addListener", "0000");
			
//			WebSocketImpl.DEBUG = true;
			
			client = new WebSocketClient(new URI(uri)) {
				
				@Override
				public void onOpen(ServerHandshake arg0) {
					// TODO Auto-generated method stub
					logger.debug("onOpen");
//					client.send(text);
				}
				
				@Override
				public void onMessage(String arg0) {
					// TODO Auto-generated method stub
					try {
						JSONObject obj = new JSONObject(arg0);
						logger.debug("[onMessage] f->name: " + funcLookupTableLocal.get(obj.getString("f")) + " " + arg0);
						if(obj.getString("f").equals(funcLookupTableRemote.get("#WSIO#addListener"))) {
							funcLookupTableRemote.put(obj.getJSONObject("d").getString("listener"), obj.getJSONObject("d").getString("alias"));
						} else {
							/*
							 * lookup function by alias from map and give it the message object as JSON
							 */
							for(Sage2EventListener listener : mapFuncNumberLocalListener.get(obj.getString("f")))
								listener.onEvent(arg0);
						}
						
						
					} catch (JSONException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					
					
				}
				
				@Override
				public void onError(Exception arg0) {
					// TODO Auto-generated method stub
					logger.debug("onError " );
					arg0.printStackTrace();
					
				}
				
				@Override
				public void onClose(int arg0, String arg1, boolean arg2) {
					// TODO Auto-generated method stub
					
				}
			};
			
			client.setSocket(instance.getSocketFactory().createSocket());

			
		} catch (NoSuchAlgorithmException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		} catch (KeyManagementException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (URISyntaxException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} 
		
		
	}
	
	private void attachListeners() {
		try {

			/*
			 * first add standard global listeners independent from any modules
			 */
			on("setupSAGE2Version", new Sage2EventListener() {

				@Override
				public void onEvent(String data) {
					onSetupSAGE2Version(data);

				}
			});

			on("initialize", new Sage2EventListener() {

				@Override
				public void onEvent(String data) {
					try {
						logger.debug("remote event: [initialize]: " + data);
						JSONObject obj = new JSONObject(data);
						String UID = obj.getJSONObject("d").getString("UID");
						setClientUID(UID);
					} catch (JSONException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
			});
			on("setupDisplayConfiguration", new Sage2EventListener() {



				@Override
				public void onEvent(String data) {
					try {
						logger.debug("remote event: [setupDisplayConfiguration]: " + data);
						conf = new Sage2DisplayConfig();
						JSONObject obj = new JSONObject(data);
						JSONObject d = obj.getJSONObject("d");

						conf.displayHost = d.getString("host");
						if(d.has("name"))
							conf.displayName = d.getString("name");
						else
							conf.displayName = conf.displayHost;

						JSONObject ui = d.getJSONObject("ui");
						conf.maxWindowSize = new Dimension(ui.getInt("maxWindowWidth"), ui.getInt("maxWindowHeight"));
						conf.minWindowSize = new Dimension(ui.getInt("minWindowWidth"), ui.getInt("minWindowHeight"));
						conf.totalWindowSize = new Dimension(d.getInt("totalWidth"), d.getInt("totalHeight"));

						for(Sage2ClientModuleInterface module : listModules)
							module.onSetupDisplayConfiguration(conf);
					} catch (JSONException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
			});
			
			/*
			 * ask modules to attach their listeners  
			 */
			for(Sage2ClientModuleInterface module : listModules)
				module.attachListeners();
			
			
		} catch (ConnectException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		
	}
	
	
	
	public void connect(String uri, String password) {
		try{
			this.uri = uri;
			this.password = password;
			init();
			
			client.connectBlocking();
			
			Executors.newSingleThreadScheduledExecutor().scheduleAtFixedRate(new RetryTimer(), 1, 2, TimeUnit.SECONDS);

			for(Sage2ClientModuleInterface module : listModules)
				module.onWebsocketClientReference(this);
			
			attachListeners();
			
			addClient();
			
			for(Sage2ClientModuleInterface module : listModules)
				module.onConnectionStatus(true);
			
		}catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ConnectException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public void close() {
		if(client == null || client.isClosed())
			return;
		try {
			for(Sage2ClientModuleInterface module : listModules)
				module.onDisconnect();
			
			client.closeBlocking();
			
			for(Sage2ClientModuleInterface module : listModules)
				module.onConnectionStatus(false);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public boolean isConnected() {
		try {
			return client != null && client.isOpen();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return false;
	}
	
	public void on(String listenerName, Sage2EventListener listener) throws JSONException, ConnectException {
		logger.debug("adding new listener: " + listenerName);
//		JSONObject addclientmsg = new JSONObject();
//		addclientmsg.put("f", "0000"/*funcLookupTableRemote.get(funcLookupTableRemote.get("#WSIO#addListener"))*/);
//
		
		/*
		 * we only attach one listener remotely.
		 * If we have multiple listeners locally listening to the same event they'll
		 * be all notified, but only one is actually received from sage2
		 */
		if(funcLookupTableLocalRev.get(listenerName) == null) {
			JSONObject d = new JSONObject();
			d.put("listener", listenerName);
			d.put("alias", String.format("%04d", ALIASCOUNTER++));


			funcLookupTableLocal.put(d.getString("alias"), listenerName);
			funcLookupTableLocalRev.put(listenerName, d.getString("alias"));

			emit("#WSIO#addListener", d);

			mapFuncNumberLocalListener.put(d.getString("alias"), new ArrayList<Sage2EventListener>());
		}
		
		mapFuncNumberLocalListener.get(funcLookupTableLocalRev.get(listenerName)).add(listener);
//		logger.debug("add listener for remote events: " + addclientmsg.toString());
//		client.send(addclientmsg.toString());
		
	}
	
	public void emit(String functionName, JSONObject msg) throws JSONException, ConnectException {
		if(!client.isOpen())
			throw new ConnectException("No connection to server. Please open one or in case it got closed, handle the closed connection");
		
		JSONObject addclientmsg = new JSONObject();
		String funcId = funcLookupTableRemote.get(functionName);
		if(funcId == null) {
			System.err.println("funcID for functionname [" + functionName + "] not found..trying later");
			listRetryEmitItems.add(new AbstractMap.SimpleEntry(functionName, msg));
		}
		else {
			addclientmsg.put("f", funcId);
			if(msg != null)
				addclientmsg.put("d", msg);
		}
		
		logger.debug("emitting: [" + functionName + "]");
		client.send(addclientmsg.toString());
	}
	
	/*
	 * API for SAGE2
	 */
	private void addClient() throws JSONException, ConnectException {
		JSONObject clientDescription = new JSONObject();
		JSONObject requests = new JSONObject();
		requests.put("config", true);
		requests.put("version", true);
		requests.put("time", false);
		requests.put("console", false);
		
		
		if(password != null)
			clientDescription.put("session", password);

		clientDescription.put("clientType", "sageUI");
		clientDescription.put("requests", requests);
		clientDescription.put("receivesWindowModification", true);
		clientDescription.put("receivesInputEvents", true);
		clientDescription.put("sendsPointerData", true);
		clientDescription.put("sendsMediaStreamFrames", true);
		clientDescription.put("requestsServerFiles", false);
		clientDescription.put("sendsWebContentToLoad", false);
		clientDescription.put("launchesWebBrowser", false);
		clientDescription.put("sendsVideoSynchonization", false);
		clientDescription.put("sharesContentWithRemoteServer", false);
		clientDescription.put("receivesDisplayConfiguration", true);
		clientDescription.put("receivesClockTime", false);
		clientDescription.put("requiresFullApps", false);
		clientDescription.put("requiresAppPositionSizeTypeOnly", false);
		clientDescription.put("receivesMediaStreamFrames", false);
		clientDescription.put("receivesPointerData", true);
		clientDescription.put("receivesRemoteServerInfo", false);

		

		emit("addClient", clientDescription);
	}

	protected void onSetupSAGE2Version(String msg){
		try {
			logger.debug("Sage2 Version Information: " + msg);
			JSONObject obj = new JSONObject(msg);
			JSONObject d = obj.getJSONObject("d");
			String base = d.getString("base");
			String branch = d.getString("branch");
			String commit = d.getString("commit");
			String date = d.getString("date");
			sage2versioninfo = new Sage2VersionInformation(base, branch, commit, date);
			
			
			for(Sage2ClientModuleInterface module : listModules)
				module.onSage2Version(sage2versioninfo);
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	

	public String getClientUUID() {
		return clientUID;
	}

	public void setClientUID(String clientUID) {
		this.clientUID = clientUID;
		
		for(Sage2ClientModuleInterface module : listModules)
			module.onInitialize(clientUID);
	}

	public Sage2VersionInformation getSage2Version() {
		return sage2versioninfo;
	}

	public void addModule(Sage2ClientModuleInterface module) {
		listModules.add(module);
	}
	
	class RetryTimer implements Runnable {
		public void run() {
			Iterator<Entry<String, JSONObject>> iterator = listRetryEmitItems.iterator();
			while(iterator.hasNext()) {
				Map.Entry<String, JSONObject> entry = iterator.next();
				listRetryEmitItems.remove(entry);
				try {
					logger.debug("retrying emit: " + entry.getKey());
					emit(entry.getKey(), entry.getValue());
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (ConnectException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
	}	
}

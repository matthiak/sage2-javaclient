package edu.monash.sage2.javaclient.structs;

import edu.monash.sage2.javaclient.helper.Dimension;


public class Sage2DisplayConfig {

	public String displayName;
	public String displayHost;
	
	
	public Dimension minWindowSize;
	public Dimension maxWindowSize;
	
	public Dimension totalWindowSize;
	
}

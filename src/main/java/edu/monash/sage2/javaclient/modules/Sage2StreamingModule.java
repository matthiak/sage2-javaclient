package edu.monash.sage2.javaclient.modules;

import java.awt.Rectangle;
import java.awt.event.InputEvent;
import java.awt.event.KeyEvent;
import java.awt.event.MouseEvent;
import java.awt.event.MouseWheelEvent;
import java.net.ConnectException;
import java.util.Base64;

import org.apache.log4j.Logger;
import org.json.JSONException;
import org.json.JSONObject;

import edu.monash.sage2.javaclient.Sage2EventListener;
import edu.monash.sage2.javaclient.Sage2WebsocketIOClient;
import edu.monash.sage2.javaclient.Sage2WebsocketIOClient.Sage2VersionInformation;
import edu.monash.sage2.javaclient.structs.Sage2DisplayConfig;

/**
 * A convenience class that sets up sage2 streaming.
 * It expects a Sage2StreamingAdapter, that will receive events
 * @author matthiak
 *
 */
public class Sage2StreamingModule implements Sage2ClientModuleInterface {

	private static final Logger logger = Logger.getLogger(Sage2StreamingModule.class);

	static int STREAMID = 1;
	

	public enum STREAMFORMATS{
		PNG,
		JPEG;

		public String toString() {
			switch(this) {
			case PNG: return "png";
			case JPEG: return "jpg";
			}
			return "";
		}
	}


	private Sage2WebsocketIOClient client;

	private Sage2StreamingAdapter adapter;

	private Sage2DisplayConfig conf;
	/**
	 * The unique ID for the stream.
	 * Used to distinguish between events for different streams, since
	 * all clients get all events for all streams
	 */
	String sage2StreamId;

	protected boolean sageMousePressed;

	protected boolean isStreaming = false;

	protected boolean requestnextframe;

	protected boolean doubleSending;

	private String uID;

	private Sage2VersionInformation versioninfo;

	public Sage2StreamingModule(Sage2StreamingAdapter adapter) {
		this.adapter = adapter;
		doubleSending = false;
	}

	
	
	
	@Override
	public void onWebsocketClientReference(Sage2WebsocketIOClient client) {
		this.client = client;
	}




	@Override
	public void onConnectionStatus(boolean connected) {
		// TODO Auto-generated method stub
		
	}




	@Override
	public void onSage2Version(Sage2VersionInformation versioninfo) {
		this.versioninfo = versioninfo;
		// TODO Auto-generated method stub
		
	}




	@Override
	public void attachListeners() throws JSONException, ConnectException {
		client.on("requestNextFrame", new Sage2EventListener() {
			@Override
			public void onEvent(String data) {
				try {
					JSONObject obj = new JSONObject(data);
					String reqStreamId = obj.getJSONObject("d").getString("streamId");
					if(reqStreamId.equals(sage2StreamId))
						adapter.onRequestNextFrame();
					requestnextframe = true;
				} catch (JSONException e) {
					e.printStackTrace();
				}

			}
		});
		client.on("eventInItem", new Sage2EventListener() {

			@Override
			public void onEvent(String data) {
				String reqStreamId;
				try {
					JSONObject obj = new JSONObject(data);
					String id = obj.getJSONObject("d").getString("id");
					reqStreamId  = extractStreamId(id);
					if(!reqStreamId.equals(sage2StreamId))
						return;
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
					return;
				}

				//					logger.debug("view: " + currentGraph.getName() + " remote event: [eventInItem]: " + data);
				MouseEvent me = null;
				int x = 0;
				int y = 0;
				try {
					JSONObject obj = new JSONObject(data).getJSONObject("d");

					JSONObject position = obj.getJSONObject("position");
					x = position.getInt("x");
					y = position.getInt("y");
					String type = obj.getString("type");
					if(type.equals("pointerMove")) {

						if(sageMousePressed) {
							me = new MouseEvent(
									adapter.getAWTComponent(), 
									MouseEvent.MOUSE_DRAGGED, 
									System.currentTimeMillis(), 
									InputEvent.BUTTON1_DOWN_MASK | InputEvent.BUTTON1_MASK, 
									x, 
									y, 
									0, 
									0, 
									1, 
									false, 
									MouseEvent.BUTTON1);	
						} else {
							me = new MouseEvent(
									adapter.getAWTComponent(), 
									MouseEvent.MOUSE_MOVED, 
									System.currentTimeMillis(), 
									0, 
									x, 
									y, 
									0, 
									0, 
									1, 
									false, 
									0);	
						}

						adapter.onMouseEvent(me);

					} else if(type.equals("pointerPress")) {
						me = new MouseEvent(
								adapter.getAWTComponent(), 
								MouseEvent.MOUSE_PRESSED, 
								System.currentTimeMillis(), 
								InputEvent.BUTTON1_DOWN_MASK | InputEvent.BUTTON1_MASK, 
								x, 
								y, 
								0, 
								0, 
								1, 
								false, 
								MouseEvent.BUTTON1);
						sageMousePressed = true;


						adapter.onMouseEvent(me);

					} else if(type.equals("pointerRelease")) {
						me = new MouseEvent(
								adapter.getAWTComponent(), 
								MouseEvent.MOUSE_RELEASED, 
								System.currentTimeMillis(), 
								0, 
								x, 
								y, 
								0, 
								0, 
								1, 
								false, 
								MouseEvent.BUTTON1);

						adapter.onMouseEvent(me);
						me = new MouseEvent(
								adapter.getAWTComponent(), 
								MouseEvent.MOUSE_CLICKED, 
								System.currentTimeMillis(), 
								0, 
								x, 
								y, 
								0, 
								0, 
								1, 
								false, 
								MouseEvent.BUTTON1);

						adapter.onMouseEvent(me);

						sageMousePressed = false;
					} else if(type.equals("pointerScroll")) {
						JSONObject wheeldata = obj.getJSONObject("data");
						int wheelDelta = wheeldata.getInt("wheelDelta") > 0 ? 1 : -1;;
						me = new MouseWheelEvent(
								adapter.getAWTComponent(), 
								MouseWheelEvent.MOUSE_WHEEL, 
								System.currentTimeMillis(), 
								0, 
								x, 
								y, 
								0, 
								0, 
								1, 
								false, 
								MouseWheelEvent.WHEEL_UNIT_SCROLL,
								wheelDelta,
								wheelDelta);
						adapter.onMouseEvent(me);
					} else if(type.equals("specialKey")) {
						JSONObject keydata = obj.getJSONObject("data");
						int code = keydata.getInt("code");
						String state = keydata.getString("state");
						int id;
						if(state.equals("down"))
							id = KeyEvent.KEY_PRESSED;
						else 
							id = KeyEvent.KEY_RELEASED;

						KeyEvent ke = new KeyEvent(adapter.getAWTComponent(), id, System.currentTimeMillis(), 0, code, (char)code);
						adapter.onKeyEvent(ke);
					}

				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}	
			}
		});
		client.on("setItemPositionAndSize", new Sage2EventListener() {

			@Override
			public void onEvent(String data) {
				try {
					logger.debug("remote event: [setItemPositionAndSize]: " + data);
					JSONObject obj = new JSONObject(data);
					JSONObject d = obj.getJSONObject("d");

					String id = obj.getJSONObject("d").getString("elemId");
					String reqStreamId  = extractStreamId(id);
					if(!reqStreamId.equals(sage2StreamId))
						return;

					int x = d.getInt("elemLeft");
					int y = d.getInt("elemTop");
					int width = d.getInt("elemWidth");
					int height = d.getInt("elemHeight");
					Rectangle windowBounds = new Rectangle(x, y, width, height);
					adapter.onSetItemPositionAndSize(windowBounds);

				} catch (NumberFormatException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		});
		client.on("finishedResize", new Sage2EventListener() {

			@Override
			public void onEvent(String data) {
				try {
					logger.debug("remote event: [finishedResize]: " + data);
					JSONObject obj = new JSONObject(data);
					String id;
					if(obj.getJSONObject("d").has("elemId"))
						id = obj.getJSONObject("d").getString("elemId");
					else
						id = obj.getJSONObject("d").getString("id");
					String reqStreamId  = extractStreamId(id);
					if(!reqStreamId.equals(sage2StreamId))
						return;
					adapter.onFinishedResize();
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		});
		client.on("stopMediaStream", new Sage2EventListener() {

			@Override
			public void onEvent(String data) {
				try {
					logger.debug("remote event: [stopMediaStream]: " + data);
					JSONObject obj = new JSONObject(data);
					String id = obj.getJSONObject("d").getString("streamId");
					String reqStreamId  = extractStreamId(id);
					if(!reqStreamId.equals(sage2StreamId))
						return;
					adapter.onStopMediaStream();
					isStreaming = false;
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		});

	}




	@Override
	public void onInitialize(String UID) {
		uID = UID;
		// TODO Auto-generated method stub
		
	}




	@Override
	public void onSetupDisplayConfiguration(Sage2DisplayConfig conf) {
		this.conf = conf;
		
	}

	public void onDisconnect() {
		stopStreaming();
	}

	public void startStreaming(final String streamName, final int width, final int height) {
		if(isStreaming)
			return;
		new Thread(new Runnable() {

			@Override
			public void run() {
				// TODO Auto-generated method stub

				while(uID == null)
					try {
						logger.debug("waiting...");
						Thread.sleep(500);
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				try {
					sage2StreamId = startMediaStream(streamName, width, height);
					isStreaming = true;
					logger.debug("started stream id=" + sage2StreamId);

					/*
					 * due to a bug on the Koolau release we need to send frames twice
					 * until they appear on the display
					 * COMMENT: moved to UI: User can select, because this can cause flickering
					 */
//					if(versioninfo.branch.equals("v1.0.0-Koolau"))
//						doubleSending = true;
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (ConnectException e) {
					// TODO Auto-generated catch block

					e.printStackTrace();
				}

			}
		}).start();
	}

	public void stopStreaming() {
		if(sage2StreamId == null)
			return;
		logger.debug("stopping stream id=" + sage2StreamId);

		try {
			stopMediaStream(sage2StreamId);
			sage2StreamId = null;
			isStreaming = false;
		} catch (ConnectException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public void updateMediaStreamFrame(final byte[] buffer) {
		if(sage2StreamId == null)
			return;
		new Thread(new Runnable() {

			@Override
			public void run() {
				try {
					while(!requestnextframe)
						try {
							Thread.sleep(20);
						} catch (InterruptedException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
					// TODO Auto-generated method stub
					requestnextframe = false;
					updateMediaStreamFrame(sage2StreamId, buffer);

					/*
					 * due to a bug on the Koolau release we need to send frames twice
					 * until they appear on the display
					 * But.. in case, this doesn't work and leads to flicker
					 * There is an option to turn this off in general
					 */
					if(doubleSending) {
						while(!requestnextframe)

							try {
								Thread.sleep(20);
							} catch (InterruptedException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}
						// TODO Auto-generated method stub
						requestnextframe = false;
						updateMediaStreamFrame(sage2StreamId, buffer);
					}
				} catch (ConnectException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}).start();
	}


	public boolean isStreaming() {
		return isStreaming;
	}

	/**
	 * request to start new media stream
	 * @param title
	 * @param width
	 * @param height
	 * @return streamid which must be used to check, if sage2 sends update request with 
	 * streamid
	 * @throws JSONException
	 */
	public String startMediaStream(String title, int width, int height)  throws JSONException, ConnectException {
		JSONObject obj = new JSONObject();
		String streamId = Integer.toString(STREAMID++);
		obj.put("id", client.getClientUUID()+"|" + streamId);
		obj.put("src", "R0lGODlhAQABAIAAAAAAAAAAACH5BAAAAAAALAAAAAABAAEAAAICTAEAOw==");
		obj.put("type", "image/gif");
		obj.put("encoding", "base64");
		obj.put("title", title);
		obj.put("width", width);
		obj.put("height", height);
		
		client.emit("startNewMediaStream", obj);
		
		return streamId;
	}


	public void updateMediaStreamFrame(String streamId, byte[] image)  throws JSONException, ConnectException {
		JSONObject obj = new JSONObject();
		
		obj.put("id", client.getClientUUID()+"|" + streamId);
		
		JSONObject state = new JSONObject();
		state.put("src", Base64.getEncoder().encodeToString(image));
		state.put("type", "image/jpg");
		state.put("encoding", "base64");
		state.put("sendToAll", true);	
		obj.put("state", state);
		
		client.emit("updateMediaStreamFrame", obj);
	}
	
	public void stopMediaStream(String streamId)  throws JSONException, ConnectException  {
		JSONObject obj = new JSONObject();
		
		obj.put("id", client.getClientUUID()+"|" + streamId);
		
		client.emit("stopMediaStream", obj);
	}
	
	
	
	public void setDoubleSending(boolean doubleSending) {
		this.doubleSending = doubleSending;
	}

	public boolean isDoubleSending() {
		return doubleSending;
	}




	private String extractStreamId(String id) {
		return id.substring(id.indexOf('|') + 1);
	}
}

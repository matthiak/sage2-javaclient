package edu.monash.sage2.javaclient.modules;

import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Rectangle;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.net.ConnectException;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

import javax.swing.JPanel;

import org.json.JSONException;
import org.json.JSONObject;

import edu.monash.sage2.javaclient.SAGE2COMMANDS;
import edu.monash.sage2.javaclient.Sage2EventListener;
import edu.monash.sage2.javaclient.Sage2WebsocketIOClient;
import edu.monash.sage2.javaclient.Sage2WebsocketIOClient.Sage2VersionInformation;
import edu.monash.sage2.javaclient.structs.Sage2DisplayConfig;

/**
 * This controller sends commands to a sage2 client (implementing Sage2ClientInterface), that
 * has already been connected to a sage2 server. 
 * @author matthiak
 *
 */
		
public class Sage2ControllerModule implements Sage2ClientModuleInterface{

	InputPanel inputPanel;
	
	Map<String, Rectangle> mapWindowIdRectangle;
	Rectangle[] orderedWindows;

	private Sage2WebsocketIOClient client;

	private Sage2VersionInformation versioninfo;

	private Sage2DisplayConfig displayconfig;

	private boolean connected;

	private MyMouseAdapter myMouseAdapter;
	
	public Sage2ControllerModule() {
		
		
		mapWindowIdRectangle = new HashMap<String, Rectangle>();
		
		inputPanel = new InputPanel();
		myMouseAdapter = new MyMouseAdapter(this);

	}
	
	
	@Override
	public void onWebsocketClientReference(Sage2WebsocketIOClient client) {
		this.client = client;
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onConnectionStatus(boolean connected) {
		this.connected = connected;
		if(connected) {
			inputPanel.addMouseListener(myMouseAdapter);
			inputPanel.addMouseMotionListener(myMouseAdapter);
		} else {
			mapWindowIdRectangle.clear();
			orderedWindows = null;
			inputPanel.repaint();
		}
	}

	@Override
	public void onSage2Version(Sage2VersionInformation versioninfo) {
		this.versioninfo = versioninfo;
		// TODO Auto-generated method stub
		
	}

	@Override
	public void attachListeners() throws JSONException, ConnectException {
		client.on("setItemPosition", new Sage2EventListener() {
			
			@Override
			public void onEvent(String data) {
				try {
					System.out.println("listener: " + "setItemPosition");
					JSONObject obj = new JSONObject(data);
					JSONObject d = obj.getJSONObject("d");
					Rectangle rectangle = mapWindowIdRectangle.get(d.getString("elemId"));
					rectangle.x = d.getInt("elemLeft");
					rectangle.y = d.getInt("elemTop");
					rectangle.width = d.getInt("elemWidth");
					rectangle.height = d.getInt("elemHeight");
					inputPanel.repaint();
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		});
		client.on("setItemPositionAndSize", new Sage2EventListener() {
			
			@Override
			public void onEvent(String data) {
				try {
					System.out.println("listener: " + "setItemPositionAndSize");
					JSONObject obj = new JSONObject(data);
					JSONObject d = obj.getJSONObject("d");
					Rectangle rectangle = mapWindowIdRectangle.get(d.getString("elemId"));
					rectangle.x = d.getInt("elemLeft");
					rectangle.y = d.getInt("elemTop");
					rectangle.width = d.getInt("elemWidth");
					rectangle.height = d.getInt("elemHeight");
					inputPanel.repaint();
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		});
		client.on("createAppWindowPositionSizeOnly", new Sage2EventListener() {
			
			@Override
			public void onEvent(String data) {
				try {
					System.out.println("listener: " + "createAppWindowPositionSizeOnly");
					JSONObject obj = new JSONObject(data);
					JSONObject d = obj.getJSONObject("d");
					Rectangle rectangle = new Rectangle();
					rectangle.x = d.getInt("left");
					rectangle.y = d.getInt("top");
					rectangle.width = d.getInt("width");
					rectangle.height = d.getInt("height");
					mapWindowIdRectangle.put(d.getString("id"), rectangle);
					
					Rectangle[] newOrderedList = new Rectangle[mapWindowIdRectangle.size()];
					int i = 0;
					if(orderedWindows != null && orderedWindows.length > 0)
						for(;i < orderedWindows.length; i++)
							newOrderedList[i] = orderedWindows[i];
					newOrderedList[i] = rectangle;
					orderedWindows = newOrderedList;
					inputPanel.repaint();
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		});
		client.on("deleteElement", new Sage2EventListener() {
			
			@Override
			public void onEvent(String data) {
				try {
					System.out.println("listener: " + "deleteElement");
					JSONObject obj = new JSONObject(data);
					JSONObject d = obj.getJSONObject("d");
					
					mapWindowIdRectangle.remove(d.getString("elemId"));
					
					synchronized (this) {
						
						orderedWindows = new Rectangle[mapWindowIdRectangle.size()];
						
						int i = 0;
						for(String remainKeys : mapWindowIdRectangle.keySet()) {
//							while(orderedWindows[i++] != null);
							orderedWindows[i++] = mapWindowIdRectangle.get(remainKeys);
						}
					}
					
					
					inputPanel.repaint();
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
			}
		});
		client.on("updateItemOrder", new Sage2EventListener() {
			
			@Override
			public void onEvent(String data) {
				try {
					System.out.println("listener: " + "updateItemOrder");
					JSONObject obj = new JSONObject(data);
					JSONObject d = obj.getJSONObject("d");

					synchronized (this) {
						orderedWindows = new Rectangle[mapWindowIdRectangle.size()];
						
						if(orderedWindows.length == 0)
							return;
						
						Set<String> keysToOrder = new HashSet<String>(mapWindowIdRectangle.keySet());

						int i = 0;

						Iterator keys = d.keys();
						while(keys.hasNext()) {
							String curKey = keys.next().toString();
							keysToOrder.remove(curKey);
							orderedWindows[d.getInt(curKey)] = mapWindowIdRectangle.get(curKey);
						}
						for(String remainKeys : keysToOrder) {
							while(orderedWindows[i++] != null);
							orderedWindows[i] = mapWindowIdRectangle.get(remainKeys);
						}
					}
					inputPanel.repaint();
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		});
	}

	@Override
	public void onInitialize(String UID) {
		try {
			JSONObject obj = new JSONObject();
			obj.put("name", "SAGE2_user");
			obj.put("color", "#ffaa00");
			
			client.emit("registerInteractionClient", obj);
		} catch (ConnectException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@Override
	public void onSetupDisplayConfiguration(Sage2DisplayConfig conf) {
		this.displayconfig = conf;
		double w = 300;
		
		double h = w * (double)conf.totalWindowSize.height / (double)conf.totalWindowSize.width;
		// TODO Auto-generated method stub
		inputPanel.setPreferredSize(new Dimension((int)w,(int)h));
		inputPanel.setMaximumSize(new Dimension((int)w,(int)h));
		inputPanel.setSize(new Dimension((int)w,(int)h));
//		inputPanel.getParent().getParent().invalidate();
	}

	@Override
	public void onDisconnect() {
		for(MouseListener ml : inputPanel.getMouseListeners())
			inputPanel.removeMouseListener(ml);
		for(MouseMotionListener mml : inputPanel.getMouseMotionListeners())
			inputPanel.removeMouseMotionListener(mml);
		
		mapWindowIdRectangle.clear();
		orderedWindows = null;
		inputPanel.repaint();
	}

	/**
	 * Sends a java mouseevent, captured in a Component wich component coordinates
	 * to sage2 and translates the coordinates to sage2 coordinates.
	 * @param me
	 */
	public void dispatchMouseEvent(MouseEvent me) { 
		if(!connected)
			return;
		Component srcComp = (Component)me.getSource();
		double compWidth = srcComp.getWidth();
		double compHeight = srcComp.getHeight();
		double relCoordX = (double)(me.getX() + 1) / compWidth; //the +1 is for small correction
		double relCoordY = (double)(me.getY() + 1) / compHeight;
	
		int sage2x = (int)(relCoordX * (double) displayconfig.totalWindowSize.width);
		int sage2y = (int)(relCoordY * (double) displayconfig.totalWindowSize.height);
		
		String button = "left";
		
//		System.out.println("dispatchMouseEvent: " + relCoordX + ", " + relCoordY);
		
		if(me.getID() == MouseEvent.MOUSE_DRAGGED) {
			sendSage2MouseMoveEvent(sage2x, sage2y);
		} else if(me.getID() == MouseEvent.MOUSE_PRESSED) {
			sendSage2MouseMoveEvent(sage2x, sage2y);
			sendSage2MousePressEvent(button);
		} else if(me.getID() == MouseEvent.MOUSE_RELEASED) {
			sendSage2MouseReleaseEvent(button);
			if(me.getClickCount() > 1)
				sendSage2MouseDblClickEvent();
		}
		
			
	}
	
	private void sendSage2MouseMoveEvent(int mousex, int mousey) {
		try {
			JSONObject obj = new JSONObject();
			obj.put("pointerX", mousex);
			obj.put("pointerY", mousey);
			
			client.emit(SAGE2COMMANDS.pointerPosition.name(), obj);
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ConnectException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	
	private void sendSage2MousePressEvent(String button) {
		try {
			JSONObject obj = new JSONObject();
			obj.put("button", button);
			
			client.emit(SAGE2COMMANDS.pointerPress.name(), obj);
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ConnectException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	private void sendSage2MouseReleaseEvent(String button) {
		try {
			JSONObject obj = new JSONObject();
			obj.put("button", button);
			
			client.emit(SAGE2COMMANDS.pointerRelease.name(), obj);
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ConnectException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	private void sendSage2MouseDblClickEvent() {
		try {

			client.emit(SAGE2COMMANDS.pointerDblClick.name(), null);
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ConnectException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	public JPanel getMouseInputPanel() {
		return inputPanel;
	}
	
	class InputPanel extends JPanel {
		
		private static final long serialVersionUID = 6008474740743064290L;

		public InputPanel() {
			// TODO Auto-generated constructor stub
		}

		
		@Override
		public void paint(Graphics g) {
			g.setColor(new Color(255,200,200));
			g.clearRect(0, 0, getWidth(), getHeight());

			if(orderedWindows == null || displayconfig == null) {
				g.setColor(new Color(220,220,220));
				g.drawLine(0, 0, getWidth(), getHeight());
				g.drawLine(0, getHeight(), getWidth(), 0);
				return;
			}

			double sage2XScale = this.getWidth() / (double) displayconfig.totalWindowSize.width;
			double sage2YScale = this.getHeight() / (double) displayconfig.totalWindowSize.height;
		
			
			for(int i = 0; i < orderedWindows.length; i ++) {
				Rectangle rectangle = orderedWindows[i];
				if(rectangle == null)
					continue;
				g.setColor(new Color(220,220,220));
				g.fillRect((int)(rectangle.x * sage2XScale), 
						(int)(rectangle.y * sage2YScale),
						(int)(rectangle.width * sage2XScale),
						(int)(rectangle.height * sage2YScale));
				g.setColor(new Color(100,100,100));
				g.drawRect((int)(rectangle.x * sage2XScale), 
						(int)(rectangle.y * sage2YScale),
						(int)(rectangle.width * sage2XScale),
						(int)(rectangle.height * sage2YScale));
			}
		}


	}
	
	
	class MyMouseAdapter extends MouseAdapter {

		private Sage2ControllerModule clientController;

		public MyMouseAdapter(Sage2ControllerModule clientController) {
			this.clientController = clientController;
			// TODO Auto-generated constructor stub
		}
		
		@Override
		public void mousePressed(MouseEvent e) {
			// TODO Auto-generated method stub
			clientController.dispatchMouseEvent(e);
		}

		@Override
		public void mouseReleased(MouseEvent e) {
			// TODO Auto-generated method stub
			clientController.dispatchMouseEvent(e);
		}

		@Override
		public void mouseMoved(MouseEvent e) {
			// TODO Auto-generated method stub
			clientController.dispatchMouseEvent(e);
		}

		@Override
		public void mouseDragged(MouseEvent e) {
			// TODO Auto-generated method stub
			clientController.dispatchMouseEvent(e);
		}
		
		
	}

}

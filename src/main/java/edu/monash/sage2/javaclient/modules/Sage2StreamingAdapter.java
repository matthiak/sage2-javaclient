package edu.monash.sage2.javaclient.modules;

import java.awt.Component;
import java.awt.Rectangle;
import java.awt.event.KeyEvent;
import java.awt.event.MouseEvent;

public interface Sage2StreamingAdapter {

	/**
	 * The AWT container, that is the source or target for events 
	 * @return
	 */
	public Component getAWTComponent();
	
	public void onRequestNextFrame();
	
	public void onMouseEvent(MouseEvent me);

	public void onKeyEvent(KeyEvent me);
	
	public void onSetItemPositionAndSize(Rectangle windowBounds);
	
	public void onFinishedResize();
	
	public void onStopMediaStream();
}

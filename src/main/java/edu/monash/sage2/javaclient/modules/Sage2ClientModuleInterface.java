package edu.monash.sage2.javaclient.modules;

import java.net.ConnectException;

import org.json.JSONException;

import edu.monash.sage2.javaclient.Sage2WebsocketIOClient;
import edu.monash.sage2.javaclient.Sage2WebsocketIOClient.Sage2VersionInformation;
import edu.monash.sage2.javaclient.structs.Sage2DisplayConfig;

public interface Sage2ClientModuleInterface {

	public void onWebsocketClientReference(Sage2WebsocketIOClient client);
	
	public void onConnectionStatus(boolean connected);

	public void onSage2Version(Sage2VersionInformation versioninfo);

	/**
	 * use this method to implement code for attaching listeners to
	 * the wsio client using the clients 'on' method
	 */
	public void attachListeners() throws JSONException, ConnectException ;
	
	public void onInitialize(String UID);
	
	public void onSetupDisplayConfiguration(Sage2DisplayConfig conf);
	
	public void onDisconnect();
}
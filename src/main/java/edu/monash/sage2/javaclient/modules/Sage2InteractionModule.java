package edu.monash.sage2.javaclient.modules;

import java.net.ConnectException;

import org.apache.log4j.Logger;
import org.json.JSONException;
import org.json.JSONObject;

import edu.monash.sage2.javaclient.Sage2WebsocketIOClient;
import edu.monash.sage2.javaclient.Sage2WebsocketIOClient.Sage2VersionInformation;
import edu.monash.sage2.javaclient.structs.Sage2DisplayConfig;

public class Sage2InteractionModule implements Sage2ClientModuleInterface {

	private static final Logger logger = Logger.getLogger(Sage2InteractionModule.class);

	
	Sage2WebsocketIOClient client;
	
	String UID;

	/*
	 * attributes for the mouse cursor
	 */
	String name;
	String color;
	
	@Override
	public void onWebsocketClientReference(Sage2WebsocketIOClient client) {
		// TODO Auto-generated method stub
		this.client = client;
	}

	@Override
	public void onConnectionStatus(boolean connected) {
		// TODO Auto-generated method stub
//		logger.debug("Connected: " + connected);
	}

	@Override
	public void onSage2Version(Sage2VersionInformation versioninfo) {
		// TODO Auto-generated method stub
//		logger.debug("Sage2 version: " + versioninfo.base);
	}

	@Override
	public void attachListeners() throws JSONException, ConnectException {
	}

	@Override
	public void onInitialize(String UID) {
		// TODO Auto-generated method stub
		this.UID = UID;
	}

	@Override
	public void onSetupDisplayConfiguration(Sage2DisplayConfig conf) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onDisconnect() {
		// TODO Auto-generated method stub
		
	}
	
	/**
	 * registering a new Interaction client (Mouse Keyboard input) with the server
	 */
	public void registerInteractionClient(String name, int r, int g, int b) {
		
		this.name = name;
		this.color = String.format("#%02x%02x%02x", r, g, b);
		
		try {
			JSONObject obj = new JSONObject();
			obj.put("name", this.name);
			obj.put("color", this.color);
			client.emit("registerInteractionClient", obj);
		} catch (ConnectException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public void startMouseCursor() {
		try {
			JSONObject obj = new JSONObject();
			obj.put("name", this.name);
			obj.put("color", this.color);
			client.emit("startSagePointer", obj);
			
			/*
			 * simulate 'toggle Pointer mode'
			 * to start interaction pointer
			 */
			obj = new JSONObject();
			obj.put("code", 16);
			client.emit("keyDown", obj);
			
			obj = new JSONObject();
			obj.put("code", 9);
			obj.put("character", String.valueOf((char)9));
			client.emit("keyPress", obj);
			
			obj = new JSONObject();
			obj.put("code", 16);
			client.emit("keyUp", obj);
		} catch (ConnectException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public void stopMouseCursor() {
		try {
			client.emit("stopSagePointer", null);
		} catch (ConnectException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public void moveMousePointer(int deltax, int deltay) {
		try {
			JSONObject obj = new JSONObject();
			obj.put("dx", deltax);
			obj.put("dy", deltay);
			client.emit("pointerMove", obj);
		} catch (ConnectException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public void pressMousePointer(int button) {
		try {
			JSONObject obj = new JSONObject();
			
			String btn = button == 1 ? "left" : button == 2 ? "middle" : "right";
			
			obj.put("button", btn);
			client.emit("pointerPress", obj);
		} catch (ConnectException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	
	public void releaseMousePointer(int button) {
		try {
			JSONObject obj = new JSONObject();
			
			String btn = button == 1 ? "left" : button == 2 ? "middle" : "right";
			
			obj.put("button", btn);
			client.emit("pointerRelease", obj);
		} catch (ConnectException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
